extends Node
class_name EntityState

var time             : float = 0
var position         : Vector3 = Vector3.ZERO
var rotation         : Vector3 = Vector3.ZERO
var linear_velocity  : Vector3 = Vector3.ZERO
var angular_velocity : Vector3 = Vector3.ZERO

func _init():
	pass # Replace with function body.

func store_state(time: float, entity : Entity):
	position         = entity.position
	rotation         = entity.get_rotation_offset()
	linear_velocity  = entity.linear_velocity
	angular_velocity = entity.angular_velocity
	self.time        = time


func apply_state(entity : Entity):
	entity.position         = position
	entity.rotation         = rotation
	entity.linear_velocity  = linear_velocity
	entity.angular_velocity = angular_velocity

func copy() -> EntityState:
	var new_state : EntityState = EntityState.new()
	new_state.time             = time
	new_state.position         = position
	new_state.rotation         = rotation
	new_state.linear_velocity  = linear_velocity
	new_state.angular_velocity = angular_velocity
	return new_state

