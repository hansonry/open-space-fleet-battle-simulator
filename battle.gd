extends Node3D
class_name Battle

@export var reference_velocity := Vector3(0, 0, 0)

var _events_to_check = []

@onready var _ships : Node3D = $Ships
@onready var _time_control = $CanvasLayer/TimeControl
@onready var _settings_ticks_per_second : int = ProjectSettings.get_setting("physics/common/physics_ticks_per_second", 60)

var tape : Tape = Tape.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MouseButton.MOUSE_BUTTON_LEFT and event.pressed:
			_events_to_check.push_back(event)
			

var _level_plane : Plane = Plane(Vector3(0, 1, 0), Vector3.ZERO)

func _process_clicks(event: InputEvent):
	var camera : Camera3D = $OrbitalCamera.camera
	var ray_normal : Vector3 = camera.project_ray_normal(event.position)
	var ray_point : Vector3 = camera.project_ray_origin(event.position)
	var point = _level_plane.intersects_ray(ray_point, ray_normal)
	if point != null:
		print("Click At: ", point)
		$Arrow.position = point
		for maybe_ship in $Ships.get_children():
			var ship : Ship = maybe_ship
			var entity: Entity = ship.get_entity()
			if ship.is_selected:
				var linear_command : Vector3 = point - entity.position
				ship.set_command_at(tape.time, linear_command, Vector3.ZERO)
	
func handle_event(event: InputEvent):
	_events_to_check.erase(event)
	
func _check_for_clicks():
	var end_event = InputEventKey.new()
	_events_to_check.push_back(end_event)
	var event: InputEvent = _events_to_check.pop_front()
	while not _events_to_check.is_empty() and event != end_event:
		_process_clicks(event)
		event = _events_to_check.pop_front()



func _time_processing(delta):
	if tape.add_engine_delta(delta):
		_time_control.time_selection = TimeControl.Selection.Pause
		for ship in _ships.get_children():
			ship.time_reset()
	
func _physics_process(delta):
	_check_for_clicks()
	_time_processing(delta)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print("Time: ", sim_time.time)
	pass

const _time_control_speed = {
	TimeControl.Selection.Reverse3: -3,
	TimeControl.Selection.Reverse2: -2,
	TimeControl.Selection.Reverse1: -1,
	TimeControl.Selection.Pause:     0,
	TimeControl.Selection.Forward1:  1,
	TimeControl.Selection.Forward2:  2,
	TimeControl.Selection.Forward3:  3,
}

func _on_time_control_time_control_changed(value : TimeControl.Selection):
	print("selection ", TimeControl.get_name_from_selection(value))
	tape.time_speed = _time_control_speed[value]
