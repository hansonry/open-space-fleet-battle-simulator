extends Node
class_name Command

var time    : float = 0
var ship    : Ship = null
var linear  : Vector3 = Vector3.ZERO
var angular : Vector3 = Vector3.ZERO


func _init():
	pass # Replace with function body.


func set_command(time : float, ship : Ship, linear : Vector3, angular : Vector3):
	self.time = time
	self.ship = ship
	self.linear = linear
	self.angular = angular

