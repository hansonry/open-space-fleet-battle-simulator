extends Node
class_name CommandList

var list : Array[Command] = []

# Called when the node enters the scene tree for the first time.
func _init():
	pass # Replace with function body.

func add_command(command : Command):
	list.push_back(command)

func get_command_at(time : float) -> Command:
	if list.size() == 0:
		return null
	var prev_command : Command = list[0]
	for command in list:
		if command.time > time:
			break
		prev_command = command
	return prev_command

func get_list_of_commands(time_from : float, time_to : float) -> Array[Command]:
	var out_array : Array[Command] = []
	if list.size() == 0:
		return out_array
	for command in list:
		if command.time > time_to:
			break
		else:
			if command.time >= time_from:
				out_array.push_back(command)
	return out_array
	
