extends Node3D
class_name OrbitalCamera

@export var scroll_wheel_change : float = 1
@export var angle_change : float = TAU / 1000

@export var zoom_min : float = 15
@export var zoom_max : float = 100
@export var zoom_start : float = 30

@export var scroll_speed : float = 100
@export var rotation_speed: float = TAU

var _pressed = false
var _target_hori : float = 0
var _target_vert : float = 0
var _target_dist : float = zoom_start
@onready var camera : Camera3D = $OrbitNode/Camera3D
@onready var orbit_node : Node3D = $OrbitNode

func _ready():
	set_process_input(true)
	_range_check_distance()

func _range_check_distance():
	_target_dist = clampf(_target_dist, zoom_min, zoom_max)

func _input(ev):
	if ev is InputEventMouseButton:
		if ev.button_index == MouseButton.MOUSE_BUTTON_RIGHT:
			_pressed = ev.pressed
			get_viewport().set_input_as_handled()
		elif ev.button_index == MouseButton.MOUSE_BUTTON_WHEEL_UP:
			_target_dist = _target_dist - scroll_wheel_change
			_range_check_distance()
			get_viewport().set_input_as_handled()
		elif ev.button_index == MouseButton.MOUSE_BUTTON_WHEEL_DOWN:
			_target_dist = _target_dist + scroll_wheel_change
			_range_check_distance()
			get_viewport().set_input_as_handled()
	# Do stuff...
	elif ev is InputEventMouseMotion:
		if _pressed:
			_target_hori = _target_hori - (ev.relative.x * angle_change)
			_target_vert = _target_vert - (ev.relative.y * angle_change)
			# Limmit Range
			_target_vert = clampf(_target_vert, -PI / 2, PI / 2)
			get_viewport().set_input_as_handled()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	camera.position.z = move_toward(camera.position.z, _target_dist, scroll_speed * delta)
	var rotTarget = Vector3(_target_vert, _target_hori, 0)
	orbit_node.rotation = orbit_node.rotation.move_toward(rotTarget, rotation_speed * delta)



