extends RigidBody3D
class_name Entity

var _entity_state_list : EntityStateList = EntityStateList.new()

var _previous_state : EntityState = EntityState.new()

var _current_hermite: HermiteState = null

var _add_time_marker : bool = true
var _previous_frame_hit : bool = false

var _rotation_counter : RotationCounter = RotationCounter.new()

var linear_thrust : Vector3 = Vector3.ZERO :
	set(value):
		if not value.is_equal_approx(linear_thrust):
			linear_thrust = value
			_add_time_marker = true

var angular_thrust : Vector3 = Vector3.ZERO:
	set(value):
		if not value.is_equal_approx(angular_thrust):
			angular_thrust = value
			_add_time_marker = true

@export var tape : Tape = null :
	set(value):
		if tape != null:
			tape.recording_change.disconnect(_on_recording_change)
		tape = value
		if value != null:
			value.recording_change.connect(_on_recording_change)

func _on_recording_change(is_recording: bool):
	if not is_recording:
		if tape.time > _entity_state_list.max_time:
			_entity_state_list.add_state_from_body(tape.time, self)
			_rotation_counter.set_rotation(rotation)
			print("Added State to List, Now sized: ", _entity_state_list.list.size())
	else:
		_rotation_counter.set_rotation(rotation)

# Called when the node enters the scene tree for the first time.
func _ready():
	# We only need to know that we have been hit
	contact_monitor = true
	max_contacts_reported = 1
	_rotation_counter.set_rotation(rotation)
	_previous_state.store_state(0, self)

func clip_time_after(time: float):
	_entity_state_list.remove_state_after(time)
	# TODO: Will need to do something after this probably

func _handle_time():
	if tape.is_recording:
		if get_contact_count() > 0:
			if not _previous_frame_hit:
				_entity_state_list.add_state(_previous_state.copy())
				_rotation_counter.set_rotation(rotation)
				print("Added State to List, Now sized: ", _entity_state_list.list.size())
			_add_time_marker = true
			_previous_frame_hit = true
		else:
			_previous_frame_hit = false
		if _add_time_marker:
			_entity_state_list.add_state_from_body(tape.time, self)
			_rotation_counter.set_rotation(rotation)
			print("Added State to List, Now sized: ", _entity_state_list.list.size())
	else:
		if _current_hermite == null or not _current_hermite.is_time_valid(tape.time):
			_current_hermite = _entity_state_list.get_hermite_at(tape.time)
		if _current_hermite == null:
			_entity_state_list.apply_last_state(self)
		else:
			_current_hermite.apply_to_entity(self, tape.time)
		
	_add_time_marker = false
	_previous_state.store_state(tape.time, self)
	#print("rot: ", rotation)

func _physics_process(delta):
	if tape.is_recording:
		_rotation_counter.update(rotation)
	_handle_time()

func get_rotation_offset() -> Vector3:
	return rotation + _rotation_counter.create_offset()

func _integrate_forces( state: PhysicsDirectBodyState3D ):
	state.apply_central_force(linear_thrust)
	state.apply_torque(angular_thrust)
	state.integrate_forces()
	

func _process(delta):
	pass
