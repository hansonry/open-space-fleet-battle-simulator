extends Control
class_name TimeControl

enum Selection {Reverse3, Reverse2, Reverse1, Pause, Forward1, Forward2, Forward3, Unknown}

@onready var reverse_3 : TextureButton = $Reverse3
@onready var reverse_2 : TextureButton = $Reverse2
@onready var reverse_1 : TextureButton = $Reverse1
@onready var pause     : TextureButton = $Pause
@onready var forward_1 : TextureButton = $Forward1
@onready var forward_2 : TextureButton = $Forward2
@onready var forward_3 : TextureButton = $Forward3

var _button_group : ButtonGroup = null

signal time_control_changed(value : TimeControl)

@export var time_selection : Selection:
	set(value):
		_set_button_selected(value)
	get:
		return _local_time_selection

var _local_time_selection : Selection = Selection.Forward1

static func get_name_from_selection(selection: Selection) -> String:
	match selection:
		Selection.Reverse3:
			return "reverse3"
		Selection.Reverse2:
			return "reverse2"
		Selection.Reverse1:
			return "reverse1"
		Selection.Pause:
			return "pause"
		Selection.Forward1:
			return "forward1"
		Selection.Forward2:
			return "forward2"
		Selection.Forward3:
			return "forward3"
	return "unknown"

func _get_button_from_selection(selection: Selection) -> TextureButton:
	match selection:
		Selection.Reverse3:
			return reverse_3
		Selection.Reverse2:
			return reverse_2
		Selection.Reverse1:
			return reverse_1
		Selection.Pause:
			return pause
		Selection.Forward1:
			return forward_1
		Selection.Forward2:
			return forward_2
		Selection.Forward3:
			return forward_3
	return null

func _get_selection_from_button() -> Selection:
	match _button_group.get_pressed_button():
		reverse_3:
			return Selection.Reverse3
		reverse_2:
			return Selection.Reverse2
		reverse_1:
			return Selection.Reverse1
		pause:
			return Selection.Pause
		forward_1:
			return Selection.Forward1
		forward_2:
			return Selection.Forward2
		forward_3:
			return Selection.Forward3
	return Selection.Unknown


func _set_button_selected(selection: Selection):
	var target_button : TextureButton = _get_button_from_selection(selection)
	target_button.button_pressed = true
	_local_time_selection = selection


# Called when the node enters the scene tree for the first time.
func _ready():
	_button_group = $Pause.button_group


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_any_button_pressed():
	_local_time_selection = _get_selection_from_button()
	time_control_changed.emit(_local_time_selection)

func _on_reverse_3_pressed():
	_on_any_button_pressed()

func _on_reverse_2_pressed():
	_on_any_button_pressed()

func _on_reverse_1_pressed():
	_on_any_button_pressed()

func _on_pause_pressed():
	_on_any_button_pressed()

func _on_forward_1_pressed():
	_on_any_button_pressed()

func _on_forward_2_pressed():
	_on_any_button_pressed()

func _on_forward_3_pressed():
	_on_any_button_pressed()
