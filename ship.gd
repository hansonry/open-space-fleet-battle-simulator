@tool
extends Node3D
class_name Ship

@export var ship_type : PackedScene = null :
	set(value):
		ship_type = value
		_on_ship_type_change()

@export var reference_velocity : Vector3 = Vector3(0, 0, 0)
@export var is_selected : bool = false : 
	set(value):
		is_selected = value
		_update_selector()

@export var initial_linear_command  : Vector3 = Vector3.ZERO
@export var initial_angular_command : Vector3 = Vector3.ZERO

const selector_color_bright : Color = Color(0.00000322476035, 0.74943345785141, 0)
const selector_color_dark   : Color = Color(0, 0.41793030500412, 0, .5)

var _command_list: CommandList = CommandList.new()

var _battle: Battle = null
var _tape : Tape = null

@onready var _selector: Sprite3D = $Selector
var _child_ship : Entity = null
var _ready_ran : bool = false


func _on_ship_type_change():
	if _child_ship != null:
		remove_child(_child_ship)
		_selector.reparent(self, false)
		_child_ship.disconnect("input_event", _click_area_input_event)
		_child_ship = null
	if ship_type != null:
		_child_ship = ship_type.instantiate()
		add_child(_child_ship)
		_child_ship.connect("input_event", _click_area_input_event)
		if _ready_ran:
			if not Engine.is_editor_hint():
				_selector.reparent(_child_ship, false)
				_child_ship.tape = _tape
				
		


func _update_selector():
	if _selector != null:
		if is_selected:
			_selector.modulate = selector_color_bright
		else:
			_selector.modulate = selector_color_dark

func _click_area_input_event(camera: Node, event: InputEvent, position: Vector3, normal: Vector3, shape_idx : int):
	if event is InputEventMouseButton:
		if event.button_index == MouseButton.MOUSE_BUTTON_LEFT and event.pressed:
			is_selected = not is_selected
			_battle.handle_event(event)
			get_viewport().set_input_as_handled()

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_ship_type_change()
	if not Engine.is_editor_hint():
		_battle = $/root/Battle
		_tape = _battle.tape
		if _child_ship != null:
				_selector.reparent(_child_ship, false)
				_child_ship.tape = _tape
		time_reset()
	_update_selector()
	_ready_ran = true

var linear_command  : Vector3 = Vector3.ZERO
var angular_command : Vector3 = Vector3.ZERO


func get_entity() -> Entity:
	return _child_ship

func set_command_at(time : float, linear : Vector3, angular : Vector3):
	var command = Command.new()
	command.set_command(time, self, linear.normalized(), angular.normalized())
	_command_list.add_command(command)


var prev_time : float = 0

func time_reset():
	prev_time = 0
	linear_command  = initial_linear_command
	angular_command = initial_angular_command

func _command_modifying():
	var time : float = _tape.time
	if prev_time <  time:
		var command_array : Array[Command] = _command_list.get_list_of_commands(prev_time, time)
		var last_command : Command = command_array.pop_back()
		if last_command != null:
			linear_command = last_command.linear
			angular_command = last_command.angular
		prev_time = time

func _physics_process(delta):
	if not Engine.is_editor_hint():
		_command_modifying()
		_child_ship.linear_thrust  = _get_linear_thrust_from_command(linear_command)
		_child_ship.angular_thrust = _get_angular_thrust_from_command(angular_command)

func _get_linear_thrust_from_command(command: Vector3) -> Vector3:
	return command * 8

func _get_angular_thrust_from_command(command: Vector3) -> Vector3:
	return command

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
