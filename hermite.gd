extends Node
class_name Hermite

var seconds : float :
	set(value):
		seconds = value
		p2 = p1 + velocity_start * value / 3
		p3 = p4 - velocity_end   * value / 3

var position_start : Vector3 :
	set(value):
		position_start = value
		p1 = value
		p2 = p1 + velocity_start * seconds / 3

var position_end   : Vector3 :
	set(value):
		position_end = value
		p4 = value
		p3 = p4 - velocity_end * seconds / 3

var velocity_start : Vector3 :
	set(value):
		velocity_start = value
		p2 = p1 + value * seconds / 3

var velocity_end   : Vector3 :
	set(value):
		velocity_end = value
		p3 = p4 - value * seconds / 3



var p1 : Vector3 :
	set(value):
		p1 = value
		#position_start = value

var p2 : Vector3 :
	set(value):
		p2 = value

var p3 : Vector3 :
	set(value):
		p3 = value

var p4 : Vector3 :
	set(value):
		p4 = value
		#position_end = value

func _init():
	pass

func _compute_k(time : float) -> float:
	return time / seconds

func get_position_at(time : float) -> Vector3:
	var k = _compute_k(time)
	return p1.bezier_interpolate(p2, p3, p4, k)

static func _quadratic_cubic_interpolate(p1 : Vector3, 
p2 : Vector3, 
p3 : Vector3, 
k : float) -> Vector3:
	var l1 = p1.lerp(p2, k)
	var l2 = p2.lerp(p3, k)
	return l1.lerp(l2, k)

func get_velocity_at(time : float) -> Vector3:
	var k = _compute_k(time)
	var v1 : Vector3 = p2 - p1
	var v2 : Vector3 = p3 - p2
	var v3 : Vector3 = p4 - p3
	return _quadratic_cubic_interpolate(v1, v2, v3, k)
