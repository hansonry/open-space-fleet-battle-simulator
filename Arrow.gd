@tool
extends Node3D
class_name Arrow

@onready var _arrow_stem: MeshInstance3D = $Base/ArrowStem
@onready var _arrow_head: MeshInstance3D = $Base/ArrowHead
@onready var _arrow_base: Node3D = $Base

@export var direction: Vector3 = Vector3(3, 0, 0) :
	set(value):
		direction = value
		_changeDirection()
		
@export var stem_diameter: float = 0.5 :
	set(value):
		stem_diameter = max(value, 0)
		_rescaleGeometry()

@export var head_diameter: float  = 1:
	set(value):
		head_diameter = max(value, 0)
		_rescaleGeometry()
@export var head_length: float = 1:
		set(value):
			head_length = max(value, 0)
			_rescaleGeometry()

@export var material : Material = null:
	set(value):
		material = value
		_set_matrial()


func _set_matrial():
	if _arrow_stem != null:
		_arrow_stem.set_surface_override_material(0, material)
		_arrow_head.set_surface_override_material(0, material)

func _changeDirection():
	if _arrow_base != null:
		_arrow_base.basis = _arrow_base.basis.looking_at(direction)
		_rescaleGeometry()

func _rescaleGeometry():
	if _arrow_base != null:
		var abs_length = direction.length()
		var stem_length = abs_length - head_length
		if stem_length <= 0:
			stem_length = 0
			_arrow_stem.hide()
		else:
			_arrow_stem.show()
		var head_scale = min(1, abs_length / head_length)
		
		#print("Stem_length ", stem_length, " head size ", head_scale )
		_arrow_stem.scale = Vector3(stem_diameter, stem_diameter, stem_length)
		_arrow_head.position = Vector3(0, 0, -stem_length)
		_arrow_head.scale = head_scale * Vector3(head_diameter, head_diameter, head_length)
		

# Called when the node enters the scene tree for the first time.
func _ready():
	_set_matrial()
	_changeDirection()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
