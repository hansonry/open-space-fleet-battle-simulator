extends Object
class_name EntityStateList

var list : Array[EntityState] = []

var max_time : float = 0

# Called when the node enters the scene tree for the first time.
func _init():
	pass # Replace with function body.

func add_state_from_body(time: float, body : Entity):
	var state = EntityState.new()
	state.store_state(time, body)
	add_state(state)

func add_state(state : EntityState):
	list.push_back(state)
	if state.time < max_time:
		push_error("Added a state with time ", state.time ," that was less than max time ", max_time)
		breakpoint
	max_time = max(max_time, state.time)
	
func apply_last_state(entity: Entity):
	var state : EntityState = list[list.size() - 1]
	state.apply_state(entity)

func _get_index_at(time : float) -> int:
	if list.size() == 0:
		return -1
	var prev_index : int = 0
	var list_size : int = list.size()
	for i in range(list_size):
		var state : EntityState = list[i]
		if state.time > time:
			return prev_index
		prev_index = i
	return list_size - 1
	

func get_state_at(time : float) -> EntityState:
	var index : int = _get_index_at(time)
	if index < 0:
		return null
	return list[index]

func remove_state_after(time: float):
	var index : int = _get_index_at(time)
	var hermite : HermiteState = _get_hermite_at_index(index)
	var state : EntityState = hermite.create_state_at(time)
	list.insert(index + 1, state)
	list.resize(index + 2)
	max_time = time

func _get_hermite_at_index(index : int) -> HermiteState:
	if index < 0 or index == list.size() - 1:
		return null
	var hermite_state : HermiteState = HermiteState.new()
	hermite_state.set_with_states(list[index], list[index + 1])
	return hermite_state

func get_hermite_at(time : float) -> HermiteState:
	var index : int = _get_index_at(time)
	#print("Getting Hermite: ", index, ", ", index + 1)
	return _get_hermite_at_index(index)

