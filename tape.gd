extends Resource
class_name Tape

var _settings_ticks_per_second : int = ProjectSettings.get_setting("physics/common/physics_ticks_per_second", 60)

signal recording_change(is_recording: bool)
signal time_speed_change(time_speed: float)

@export var is_recording : bool = true :
	set(value):
		is_recording = value
		recording_change.emit(value)

@export var time_speed : float = 1:
	set(value):
		time_speed = value
		var abs_time_speed = abs(value)
		Engine.time_scale = abs_time_speed
		Engine.physics_ticks_per_second = _settings_ticks_per_second * abs_time_speed
		time_speed_change.emit(value)
		if value < 0:
			is_recording = false
@export var time : float = 0:
	set(value):
		time = value

var max_time : float = 0

func compute_time_delta(delta: float):
	return delta * time_speed

func add_engine_delta(delta: float) -> bool:
	var direction = sign(time_speed)
	time += delta * direction
	if not is_recording and time > max_time:
		is_recording = true
	max_time = max(max_time, time)
	if time < 0:
		time = 0
		return true
	return false

func _init():
	pass
