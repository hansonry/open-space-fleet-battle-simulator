extends Node
class_name RotationCounter

var _previous_rotation : Vector3 = Vector3.ZERO
var _count_x : int = 0
var _count_y : int = 0
var _count_z : int = 0

# Called when the node enters the scene tree for the first time.
func _init():
	pass # Replace with function body.

func reset():
	_count_x = 0
	_count_y = 0
	_count_z = 0

func set_rotation(rotation: Vector3):
	_previous_rotation = rotation
	reset()

func _compute_offset(prev : float, now : float) -> int:
	var hpi : float = PI / 2
	if prev < -hpi and now > hpi:
		return -1
	elif prev > hpi and now < -hpi:
		return 1
	return 0

func update(rotation: Vector3):
	_count_x += _compute_offset(_previous_rotation.x, rotation.x )
	_count_y += _compute_offset(_previous_rotation.y, rotation.y )
	_count_z += _compute_offset(_previous_rotation.z, rotation.z )
	_previous_rotation = rotation
	#print("(", _count_x, ", ", _count_y, ", ", _count_z, ")")
	#print(rotation)

func create_offset() -> Vector3:
	return Vector3(_count_x * TAU, _count_y * TAU, _count_z * TAU)

