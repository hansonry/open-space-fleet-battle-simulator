extends Node
class_name HermiteState

var start_time : float = 0
var linear     : Hermite = Hermite.new()
var angular    : Hermite = Hermite.new()

func _init():
	pass

func _normalize_rotation(vect : Vector3) -> Vector3:
	return Vector3(fmod(vect.x + PI, TAU) - PI, fmod(vect.y + PI, TAU) -PI ,fmod(vect.z + PI, TAU) - PI)
	

func _get_rotation(t : float) -> Vector3:
	return _normalize_rotation(angular.get_position_at(t))

func apply_to_entity(entity: Entity, time: float):
	var t = time - start_time
	entity.position = linear.get_position_at(t)
	entity.rotation = _get_rotation(t)

func create_state_at(time : float) -> EntityState:
	var state : EntityState = EntityState.new()
	var t = time - start_time
	state.position         = linear.get_position_at(t)
	state.linear_velocity  = linear.get_velocity_at(t)
	state.rotation         = _get_rotation(t)
	state.angular_velocity = angular.get_velocity_at(t)
	
	return state


func is_time_valid(time : float) -> bool:
	return time >= start_time and time < start_time + linear.seconds

func set_with_states(start : EntityState, end : EntityState):
	var dt = end.time - start.time
	start_time = start.time
	
	linear.seconds        = dt
	linear.position_start = start.position
	linear.position_end   = end.position
	linear.velocity_start = start.linear_velocity
	linear.velocity_end   = end.linear_velocity
	
	angular.seconds        = dt
	angular.position_start = _normalize_rotation(start.rotation)
	angular.position_end   = end.rotation
	angular.velocity_start = start.angular_velocity
	angular.velocity_end   = end.angular_velocity
	
	#print("chaange:", start.rotation, angular.position_start)
	#print("delta rot: ", angular.position_end  - angular.position_start, " Original ", end.rotation, ", ", start.rotation)

